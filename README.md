# jointhefedi.com

Landing page for the Fediverse.

## License

Based on [Hyperspace](https://html5up.net/hyperspace) by HTML5 UP, this site and its contents (except third-party logos) are licensed under CC-BY 3.0.
See the LICENSE.txt for the full license.
